package net.zerodind.uhccore.nms.v1_19_R1;

import java.util.OptionalInt;

import com.mojang.serialization.Lifecycle;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.DensityFunctions;
import net.minecraft.world.level.levelgen.DensityFunctions.MarkerOrMarked;
import net.zerodind.uhccore.nms.NmsAdapter;
import net.zerodind.uhccore.nms.NmsOperationException;

public class NmsAdapterImpl implements NmsAdapter {

	@Override
	public void removeOceans() throws NmsOperationException {
		// See net.minecraft.world.level.levelgen.NoiseRouterData.CONTINENTS
		final ResourceKey<DensityFunction> continentsKey = ResourceKey.create(
			Registry.DENSITY_FUNCTION_REGISTRY, new ResourceLocation("overworld/continents"));

		RegistryUtils.write(Registry.DENSITY_FUNCTION_REGISTRY, dfRegistry -> {
			// See net.minecraft.world.level.levelgen.NoiseRouterData.bootstrap()
			final MarkerOrMarked continents;
			try {
				continents = (MarkerOrMarked) dfRegistry.getOptional(continentsKey)
					.orElseThrow(() -> new NmsOperationException("Missing continents density function"));
			} catch (ClassCastException e) {
				throw new NmsOperationException(e);
			}
			// We keep the flatCache for performance, but take the absolute value of the noise function.
			// This stops ocean generation, but keeps the qualities of the original noise.
			final DensityFunction continentsWithoutOceans = DensityFunctions.flatCache(continents.wrapped().abs());
			dfRegistry.registerOrOverride(OptionalInt.empty(), continentsKey,
				continentsWithoutOceans, Lifecycle.stable());
		});
	}

}
