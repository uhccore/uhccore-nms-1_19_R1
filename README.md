# UhcCore NMS 1_19_R1

This project contains a [UhcCore NMS][1] adapter implementation
for Spigot NMS version `1_19_R1`.

NMS is short for `net.minecraft.server`, a package which contains
internal implementation details of the Minecraft server.
Usage of NMS should be avoided at all costs, because it is not an API and
will change frequently between each Minecraft version. This means that
adapters will need to be maintained for each supported Minecraft version.

[1]: https://gitlab.com/uhccore/uhccore-nms

## License

```
Copyright (C) 2022 Odin Dahlström

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
